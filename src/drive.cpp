#include "include/drive.hpp"

char ArduinoIsStupid = 255;

Drive::Drive(int step, int dir, int rf_rx, int rf_tx,
	int marker_num){
	digitalWrite(13, HIGH);

	this->marker_num = marker_num;
	motor = new DRV8834(STEPS, dir, step);
	motor->setMicrostep(1);

	cereal = new SoftwareSerial(rf_rx, rf_tx);
	rf = new enes100::RfClient<SoftwareSerial>(cereal);
	cereal->begin(9600);
	digitalWrite(13, LOW);
	marker.x = 0;
	marker.y = 0;

}

void Drive::setSpeed(int left, int right){
	Serial.println("begin transmission");
	Wire.beginTransmission(slave_addr);
	Serial.println("begun");
	//litte endian data transfer
	//lsb first
	Wire.write((byte)left & 0xFF);
	Serial.println("first written");
	Wire.write((byte)(left) >> 8);
	Serial.println("second written");
	Wire.endTransmission();
	Serial.println("trasnmission done");
	this->motor->setRPM(abs(right));
	this->motor->rotate(-steps_per_sec * right);

}

void Drive::setDesiredPos(float x, float y){
	desiredLoc.x = x;
	desiredLoc.y = y;
}

void Drive::tickMove(){
	static enes100::Marker lastpos;
	static long lastTime = millis();
	static double ix, iy, ia;
	//digitalWrite(13,HIGH);
	enes100::Marker currpos = getMarker();
	//units of seconds
	double dTime = (millis() - lastTime) / 1000;
	if(dTime == 0) return;

	//units of m
	double ex = currpos.x - desiredLoc.x;
	double ey = currpos.y - desiredLoc.y;

	//angle from current pos to destination
	double angle_to_dest = atan2(ey, ex);
	double ea = currpos.theta - angle_to_dest;

	//units of m * seconds
	ix += ex * dTime;
	iy += ey * dTime;

	ia += ea * dTime;

	//units of m / seconds
	double dx =  (currpos.x - lastpos.x) / dTime;
	double dy = (currpos.y - lastpos.y) / dTime;

	double da = (currpos.theta - lastpos.theta) / dTime;

	double v_x = kP * ex + kI * ix + kD * dx;
	double v_y = kP * ey + kI * iy + kD * dy;
	double w = kPr * ea + kIr * ia + kDr * da;

	/**
	 * Wheels x, y (mm)
	 * |(0, 264) | (320, 264)
	 * | (0,0)	| (320, 0)
	 */

	double cur_cos = cos(currpos.theta);
	double cur_sin = sin(currpos.theta);

	double v_l = v_x * cur_cos + v_y * cur_sin + (w * X_OFFSET);
	double v_r = v_x * cur_cos + v_y * cur_sin - (w * X_OFFSET);

	int left = v_l / RPM_TO_SPEED;
	int right = v_r / RPM_TO_SPEED;
	left = (left > MAX_SPEED) ? MAX_SPEED : left;
	left = (left < -MAX_SPEED) ? -MAX_SPEED : left;
	right = (right > MAX_SPEED) ? MAX_SPEED : right;
	right = (right < -MAX_SPEED) ? -MAX_SPEED : right;

	sendMessage(String(right));
	setSpeed(left, right);

	lastpos = currpos;
	lastTime = millis();

}

void Drive::setDesiredRot(float rot){
	desiredLoc.theta = rot;
}

void Drive::tickRot(){
	static float lastAngle;
	static long lastTime = millis();
	static double ia;

	enes100::Marker currpos = getMarker();
	double dTime = (millis() - lastTime) / 1000;
	if(dTime == 0) return;
	double ea = desiredLoc.theta - currpos.theta;

	ia += ea * dTime;

	double da = (currpos.theta - lastAngle) / dTime;

	double w = kPr * ea + kIr * ia + kDr * da;

	double v_l =  (w * X_OFFSET);
	double v_r = -(w * X_OFFSET);

	int left = v_l / RPM_TO_SPEED;
	int right = v_r / RPM_TO_SPEED;
	left = (left > MAX_SPEED) ? MAX_SPEED : left;
	left = (left < -MAX_SPEED) ? -MAX_SPEED : left;
	right = (right > MAX_SPEED) ? MAX_SPEED : right;
	right = (right < -MAX_SPEED) ? -MAX_SPEED : right;

	setSpeed(left, right);
}

bool Drive::closeEnough(float x, float y, float tolerance){
		return (abs(x - getMarker().x)) < tolerance && (abs(y - getMarker().y)) < tolerance;
}

enes100::Marker Drive::getMarker(){
	return marker;
}

void Drive::recMarker(){
	rf->receiveMarker(&marker, marker_num, TIMEOUT);
}

void Drive::startRfServer(){
	rf->resetServer();
	rf->sendMessage("Robot is operational.\n");
}

void Drive::receiveData(int bytes){
	static byte rec[2];
	digitalWrite(13, HIGH);
	// if(bytes != 2)
	// 	//wat.
	// 	return;
	//little endian data transfer
	rec[0] = Wire.read();
	rec[1] = Wire.read();
	int i = (rec[1] << 8);
	i = i | rec[0];
	Serial.println(i);
	digitalWrite(13,LOW);
	this->motor->setRPM(abs(i));
	this->motor->rotate(steps_per_sec * i);

}

bool Drive::startI2C(void(*callback)(int)){
	if(digitalRead(A3) == HIGH){
		//implies we're the slave
		Wire.begin(slave_addr);
		Wire.onReceive(callback);
		return true;
	}else{
		Wire.begin();
		return false;
	}

}

void Drive::sendMessage(String s){
	rf->sendMessage(s);
}
