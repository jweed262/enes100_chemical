#include <DRV8834.h>
#include <enes100.h>
#include "include/drive.hpp"
#include <TimerOne.h>

#define step 5
#define dir 4
#define rx 8
#define tx 9
#define mark_num 20

char WtfArduino = 255;
int state = 0, nextState = 0;

Drive driver(step, dir, rx, tx, mark_num);

void setup(){
	digitalWrite(13,HIGH);
	delay(500);
	driver.startRfServer();
	if(driver.startI2C(callback)){
		state = -1;
	}
	driver.sendMessage("OSV online");
	digitalWrite(13,LOW);
	driver.setDesiredPos(1,1);
	Serial.begin(9600);
}

void loop(){
	if(state != -1){
		digitalWrite(13,HIGH);
		//driver.recMarker();
		//enes100::Marker currpos = driver.getMarker();
		driver.setSpeed(100, 100);
		digitalWrite(13, LOW);
		delay(100);
	}


}

void stateMachine(){
	switch(state){
		case -1:
			//this means we're the slave motor controller
			break;
		case 0:
			//move to the edges of the obstacle
			driver.recMarker();
			if(driver.getMarker().y > 1.0){
				driver.setDesiredPos(.5, 1.75);
			}else{
				driver.setDesiredPos(5, .25);
			}
			state = 1;
			nextState = 2;
			break;
		case 1:
			driver.recMarker();
			driver.tickMove();
			if(driver.closeEnough(driver.desiredLoc.x, driver.desiredLoc.y, .07)){
				state = nextState;
			}
			break;
		case 2:
			//determine if the wall is present
			state = 3;
			break;
		case 3:
			//get rotate
			driver.setDesiredRot(0);
			state = 4;
			nextState = 5;
			break;
		case 4:
			driver.recMarker();
			driver.tickRot();
			if(abs(driver.getMarker().theta - driver.desiredLoc.theta) < .1){
				state = nextState;
			}
			break;
		case 5:
			//move outside of wall
			driver.setDesiredPos(1.5, driver.getMarker().y);
			state = 1;
			nextState = 6;
			break;
		case 6:
			//move to pool
			driver.setDesiredPos(2, 1.5);
			state = 1;
			nextState = 7;
			break;
		case 7:
			//neutralize
			break;
	}
}

void callback(int bytes){
	driver.receiveData(bytes);
}
