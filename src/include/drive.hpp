#ifndef DRIVE
#define DRIVE

#include <Arduino.h>
#include <DRV8834.h>
#include <enes100.h>
#include <math.h>
#include <Wire.h>


#define sign(x) ((x > 0) - (x < 0))

class Drive{
protected:
	//PID constants
	int kP = 200,
		kI = 1,
		kD = 0,
		kPr = 1,
		kIr = 0,
		kDr = 0;
	//pointers to the motors
	DRV8834 *motor;
	//steps/rotation for our motors
	const int STEPS = 200;
	//number of ticks to move each iteration.
	const double steps_per_sec = 1.0 / 60.0 * STEPS;
	//number for our marker - should be on the back of the board
	int marker_num;
	//marker retrieved from the rf system
	enes100::Marker marker;
	//serial implementation for the rf
	SoftwareSerial *cereal;
	//the rf
	enes100::RfClient<SoftwareSerial> *rf;
	//scaling factor to convert from rpm to m/s
	const double RPM_TO_SPEED = 0.32986;
	//offset from center of the wheels, in the x direction, in m
	const double X_OFFSET = .160;
	//Manual timeout for getting a marker, in ms
	const int TIMEOUT = 600;
	//address of the slave device in I2C
	const int slave_addr = 8;
	//maximum speed of the motors
	const int MAX_SPEED = 75;

public:
	//marker representing the location we want to be in
	enes100::Marker desiredLoc;
	
	/**
	 * Constructor for the drive system
	 */
	Drive(int step, int dir, int rf_rx, int rf_tx,
		int marker_num);

	/**
	 * sets the speed of both motors, given in rpm
	 * @param left  rpm of left motor
	 * @param right rpm of right motor
	 */
	void setSpeed(int left, int right);

	/**
	 * Like a game engine tick, this advances the robot's drive system to the
	 * next descrete point in time, and recalculates the PID control values
	 * only ticks the x,y movement
	 */
	void tickMove();
	/**
	 * The same as <code>tickMove()</code>, but ticks rotation
	 */
	void tickRot();
	/**
	 * sets the destination of the robot, to be used in PID
	 * @param x x value, in meters, of the desired position
	 * @param y y-value, in meters, of the desired position
	 */
	void setDesiredPos(float x, float y);

	/**
	 * Similar to setDesiredPos, this sets our desired rotation.
	 * Probably only useful when we want to rotate in place
	 * @param rot angle, in rad (not sure)
	 */
	void setDesiredRot(float rot);

	/**
	 * returns a new location marker from the rf system
	 * returns the old one if we don't get anything.
	 * The rf system is -blocking-, so it may not be too smart to call this
	 * from a time-sensitive loop :/
	 * @return marker of current pos or last marker if unable to retrieve
	 */
	enes100::Marker getMarker();

	/**
	 * pulls a new marker from the rf system.
	 * if the call takes longer than TIMEOUT,
	 * nothing changes
	 *
	 */
	void recMarker();

	/**
	 *	starts the rf server, to allow delays to be handled in the .ino
	 */
	void startRfServer();

	/**
	 * sends a message to the rf server
	 * @param s message to send
	 */
	void sendMessage(String s);

	/**
	 * starts the I2C connection between the two Arduinos.
	 * Assumes that the slave will have A3 tied to high, and master should be floating
	 * @return returns true if the device is determined to be the slave
	 */
	bool startI2C(void(*callback)(int));

	/**
	 * receives data transmitted over I2C
	 * @param bytes number of bytes recieved
	 */
	void receiveData(int bytes);

	/**
	 * determines if we're close enough to the given point
	 * @param  x         x value of the goal location
	 * @param  y         y value of the goal location
	 * @param  tolerance tolerance within which we must be of the goal point
	 * @return           if we're close enough
	 */
	bool closeEnough(float x, float y, float tolerance);
};

#endif
