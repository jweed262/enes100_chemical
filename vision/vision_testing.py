#!/usr/bin/env python

import numpy as np
import cv2
from matplotlib import pyplot as plt

img = cv2.imread("test1.jpg", 0)
corn = cv2.goodFeaturesToTrack(img, 25, 0.01, 10)
corn = np.int0(corn)

for i in corn:
	x, y =i.ravel()
	cv2.circle(img, (x,y), 3, 255, -1)
plt.imshow(img), plt.show()
cv2.waitKey(0)
cv2.destroyAllWindows()
